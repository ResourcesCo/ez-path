use serde_json;

pub fn parse(path: &str) -> Result<Vec<String>, &'static str> {
    let mut result = Vec::new();
    let mut current = String::from("");
    let mut multi = false;
    for s in path.split("/") {
        if multi {
            current.push_str("/");
            current.push_str(s);
        } else if s.get(0..1) == Some("\"") {
            current.push_str(s);
            multi = true;
        } else {
            let item: String = s.into();
            result.push(item);
        }

        if multi {
            let len = current.len();
            if current.get(len-1..) == Some("\"") {
                let item: String = current.clone().into();
                let json_result: std::result::Result<String, serde_json::Error> = serde_json::from_str(&item);
                match json_result {
                    Ok(item) => result.push(item),
                    Err(_err) => return Err("Invalid path"),
                }
                current.drain(..);
                multi = false;
            }
        }
    }
    if !multi {
        return Ok(result)
    }
    return Err("Invalid path")
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn parse_unquoted() {
        assert_eq!(parse("a/b/c").unwrap(), ["a", "b", "c"]);
    }

    #[test]
    fn parse_quoted() {
        assert_eq!(parse("a/\"b/b\"/c").unwrap(), ["a", "b/b", "c"]);
    }
}
